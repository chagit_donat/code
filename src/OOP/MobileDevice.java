package OOP;

public class MobileDevice
{
    private String model;
    private char os;
    private double OSVersion;
    private boolean hashFlash;
    private int price;
    private int discount;
    final int vat = 17;
    static int maxPrice = 1000;


    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public char getOs()
    {
        return os;
    }

    public void setOs(char os)
    {
        this.os = os;
    }


    public void printParameters()
    {
        System.out.println(getModel() + " " + getOs() + " " + OSVersion + " " + hashFlash+ " " + price);
    }

    public static void changeMaxPrice(int newPrice)
    {
        maxPrice = newPrice;
    }

    public int caculatePrice(int price)
    {
        return  price;
    }

    public int caculatePrice(int price, int discount)
    {
        return price - discount;
    }

}
